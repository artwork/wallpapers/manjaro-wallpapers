import yaml
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

import glob
import os
from pathlib import Path
import shutil
from PIL import Image
from optparse import OptionParser
from xml.etree import ElementTree as ET
from xml.dom import minidom

def common_pre():
    Path(f"{options.dirname}/usr/share/manjaro-wallpapers/").mkdir(parents=True, exist_ok=True)

def common_process(filename):
    shutil.copy2(f"{os.path.dirname(filename)}/image.png", f"{options.dirname}/usr/share/manjaro-wallpapers/{os.path.basename(os.path.dirname(filename))}.png")
    print(f"=====\n[Common] Copying '{os.path.dirname(filename)}' as {options.dirname}/usr/share/manjaro-wallpapers/{os.path.basename(os.path.dirname(filename))}.png")

def plasma_write_desktop_file(filename, metadata):
    desktop = f"""[Desktop Entry]
Name={os.path.basename(os.path.dirname(filename))}
X-KDE-PluginInfo-Name={os.path.basename(os.path.dirname(filename))}
X-KDE-PluginInfo-Author={metadata['author']}
X-KDE-PluginInfo-License={metadata['license']}
    """

    with open(f"{options.dirname}/usr/share/wallpapers/{os.path.basename(os.path.dirname(filename))}/metadata.desktop", "w") as text_file:
        text_file.write(desktop)

def plasma_process(filename):
    stream = open(filename, 'r')
    metadata = yaml.load(stream, Loader)

    Path(f"{options.dirname}/usr/share/wallpapers/{os.path.basename(os.path.dirname(filename))}/contents/images/").mkdir(parents=True, exist_ok=True)

    relpath = os.path.relpath(f"{options.dirname}/usr/share/manjaro-wallpapers/{os.path.basename(os.path.dirname(filename))}.png", f"{options.dirname}/usr/share/wallpapers/{os.path.basename(os.path.dirname(filename))}/contents/images/")

    img = Image.open(f"{os.path.dirname(filename)}/image.png")
    width, height = img.size
    os.symlink(relpath, f"{options.dirname}/usr/share/wallpapers/{os.path.basename(os.path.dirname(filename))}/contents/images/{width}x{height}.png")
    print(f"[Plasma] Processing '{os.path.basename(os.path.dirname(filename))}' as {options.dirname}/usr/share/wallpapers/{os.path.basename(os.path.dirname(filename))}/")

    plasma_write_desktop_file(filename, metadata)


def gnome_xml_file_subelement(filename, wallpapers_xml):
    wallpaper = ET.SubElement(wallpapers_xml, "wallpaper")
    ET.SubElement(wallpaper, "name").text = os.path.basename(os.path.dirname(filename))
    ET.SubElement(wallpaper, "filename").text = f"/usr/share/backgrounds/manjaro-wallpapers/{os.path.basename(os.path.dirname(filename))}.png"

    ET.SubElement(wallpaper, "options").text = "zoom"
    ET.SubElement(wallpaper, "shade_type").text = "solid"
    ET.SubElement(wallpaper, "pcolor").text = "#ffffff"
    ET.SubElement(wallpaper, "scolor").text = "#000000"

def gnome_pre():
    Path(f"{options.dirname}/usr/share/backgrounds/manjaro-wallpapers/").mkdir(parents=True, exist_ok=True)
    return ET.Element('wallpapers')


def gnome_process(filename, wallpapers_xml):

    relpath = os.path.relpath(f"{options.dirname}/usr/share/manjaro-wallpapers/{os.path.basename(os.path.dirname(filename))}.png", f"{options.dirname}/usr/share/backgrounds/manjaro-wallpapers/")

    os.symlink(relpath, f"{options.dirname}/usr/share/backgrounds/manjaro-wallpapers/{os.path.basename(os.path.dirname(filename))}.png")
    print(f"[GNOME] Processing '{os.path.basename(os.path.dirname(filename))}' as {options.dirname}/usr/share/backgrounds/manjaro-wallpapers/{os.path.basename(os.path.dirname(filename))}.png")
    gnome_xml_file_subelement(filename, wallpapers_xml)


def gnome_post(wallpapers_xml):
    with open(f"{options.dirname}/usr/share/backgrounds/manjaro-wallpapers/manjaro-wallpapers.xml", "w") as text_file:
        pretty_xml = minidom.parseString(ET.tostring(wallpapers_xml, encoding="unicode")).toprettyxml(indent="   ")
        text_file.write(pretty_xml)

def process():
    common_pre()
    wallpapers_xml = gnome_pre()

    for filename in glob.iglob(f"{options.inputdir}/**/metadata.yml"):
        common_process(filename)
        gnome_process(filename, wallpapers_xml)
        plasma_process(filename)

    gnome_post(wallpapers_xml)


parser = OptionParser()

parser.add_option("-o", "--output", dest="dirname",
                  help="Write result structure to INSTALLDIR", metavar="INSTALLDIR")

parser.add_option("-i", "--input", dest="inputdir",
                  help="Read source structure from INPUTDIR", metavar="INPUTDIR")

(options, args) = parser.parse_args()

process()
